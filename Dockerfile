ARG DEBIAN_VERSION
ARG GCC_VERSION
ARG PYTHON_VERSION
ARG MESON_VERSION

FROM python:${PYTHON_VERSION}-slim-${DEBIAN_VERSION} as builder

ARG MESON_VERSION

RUN python3 -m pip install -U --no-cache-dir \
        pip \
        setuptools \
        wheel && \
    python3 -m pip wheel -w /wheelhouse --no-cache-dir \
        meson==${MESON_VERSION}


FROM gcc:${GCC_VERSION}-${DEBIAN_VERSION}

ARG MESON_VERSION

COPY --from=builder /wheelhouse /wheelhouse
COPY venv_entrypoint.sh /usr/local/bin/
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        ninja-build \
        python3-pip \
        python3-venv && \
    rm -rf /var/lib/apt/lists/* && \
    python3 -m venv /venv && \
    /venv/bin/python3 -m pip install -U --no-cache-dir \
        pip \
        setuptools && \
    /venv/bin/python3 -m pip install --no-index --find-links=/wheelhouse --no-cache-dir \
        meson==${MESON_VERSION} && \
    rm -r /wheelhouse

ENTRYPOINT ["venv_entrypoint.sh"]
CMD ["meson"]
